/*
  le JSON ressemble à ça :

  characterList: [
    {
      name: "Harry Potter",
      alternate_names: [
        "The Boy Who Lived",
        "The Chosen One",
        "Undesirable No. 1",
        "Potty"
      ]
    },
    {
      name: "Hermione Granger",
      alternate_names: [
        "Hermy",
        "Know-it-all",
        "Miss Grant",
        "Herm-own-ninny"
      ]
    },
    ...
  ]
*/
fetch("https://hp-api.onrender.com/api/characters")
  .then((response) => response.json())
  .then((characterList) => {
    // on crée le conteneur de tous les personnages
    const characterListContainer = document.createElement('ul');
    // pour chaque personnage de la liste
    for (const eachCharacter of characterList) {
      // on crée le conteneur d'un seul personnage
      const characterContainer = document.createElement('li');
      // on crée le nom du personnage
      const characterName = document.createElement('h3');
      characterName.textContent = eachCharacter.name;
      // on ajoute le nom au personnage
      characterContainer.appendChild(characterName);

      // on crée une liste des noms alternatifs
      const characterAlternateNames = document.createElement('ul');
      // pour chaque nom alternatif dans le tableau
      for (const alternateName of eachCharacter.alternate_names) {
        const alternateNameContainer = document.createElement('li');
        alternateNameContainer.textContent = alternateName;
        // on ajoute le nom alternatif à la liste
        characterAlternateNames.appendChild(alternateNameContainer);
      }
      // on ajoute la liste des noms alternatifs au personnage
      characterContainer.appendChild(characterAlternateNames);

      // on ajoute le personnage à la liste
      characterListContainer.appendChild(characterContainer);
    }

    // on récupère le contenant principal de la page
    const root = document.getElementById('root');
    // on ajoute la liste au document
    root.appendChild(characterListContainer);
  });